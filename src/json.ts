/**
 * File reading module.
 * Read  : fs.readFileSync(path);
 * Write : fs.writeFileSync(path, string);
 */
import * as fs from 'fs';

interface Data {
	name  : string;
	email : string;
	uuid  : string;
}

/* Read data in file */

/* Update data name & email */

/* Write new data in file */
