/**
 * job2 function receives a parameter data.
 * You must modify the code below based on the following rules:

 * Your function must always return a promise.
 * If data is not a number,
 *     return a promise rejected instantly and give the data "error" (string).
 *
 * If data is an odd number,
 *     return a promise resolved 1 second later and give the data "odd" (string).
 *
 * If data is an even number,
 *     return a promise rejected 2 seconds later and give the data "even" (string).
 *
 * @help : to check if data is not a number, you can use the function isNaN(data).
 */

function job2(data) {
    return data;
}

console.log(job2(14));
